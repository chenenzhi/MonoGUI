// stdafx.cpp : source file that includes just the standard includes
//	DTEditor.pch will be the pre-compiled header
//	stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

// 从一行文字中取得两个分号之间的片断
BOOL GetSlice( CString* pstrReturn, CString strInput, int k )
{
	int nStartPos = 0;
	int nEndPos = -1;

	for( int i=0; i<=k; i++ )
	{
		// 把上次查找的位置给开始位置
		nStartPos = nEndPos+1;

		// 查找分隔符';'
		nEndPos = strInput.Find( ';', nStartPos );

		if( nEndPos == -1 )
		{
			return FALSE;
		}
	}
 
	// 将nStartPos和nEndPos之间的内容拷贝给temp
	*pstrReturn = strInput.Mid( nStartPos, nEndPos-nStartPos );
	return TRUE;
}

// 从一段文字中取出一行
BOOL GetLine( CString* pstrReturn, CString strInput, int k )
{
	int nStartPos = 0;
	int nEndPos = -1;

	for( int i=0; i<=k; i++ )
	{
		// 把上次查找的位置给开始位置
		nStartPos = nEndPos+1;

		// 查找分隔符'\r'
		nEndPos = strInput.Find( '\r', nStartPos );

		if( nEndPos == -1 )
		{
			return FALSE;
		}
	}
 
	// 将nStartPos和nEndPos之间的内容拷贝给temp
	CString strTemp = strInput.Mid( nStartPos, nEndPos-nStartPos );

	// 如果开始字符是“\n”，则去掉
	if (strcmp (strTemp.Left(1), "\n") == 0)
	{
		strTemp = strInput.Mid( nStartPos+1, nEndPos-nStartPos-1 );
	}

	*pstrReturn = strTemp;
	return TRUE;
}

/* END */