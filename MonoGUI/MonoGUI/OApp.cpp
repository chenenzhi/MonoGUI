////////////////////////////////////////////////////////////////////////////////
// @file OApp.cpp
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#include "MonoGUI.h"

OApp::OApp()
{
	m_bReverseShow = FALSE;

	m_pLCD        = new LCD();
	m_pScreenBuffer = new LCD(SCREEN_W, SCREEN_H);
	m_pMsgQ       = new OMsgQueue();
	m_pMainWnd    = NULL;
	m_pCaret      = new OCaret();
	m_pTimerQ     = new OTimerQueue();
	m_pTopMostWnd = NULL;
	m_pKeyMap     = new KeyMap();
	m_pClock      = new OClock(this);
	m_pSysBar     = new OSystemBar();
	m_pImageMgt   = new BWImgMgt();

#if defined (CHINESE_SUPPORT)
	m_pIMEWnd     = new OIME();
#endif // defined(CHINESE_SUPPORT)

	m_bClockKeyEnable = TRUE;

	m_fnEventProcess = NULL;
}

OApp::~OApp()
{
#if defined (RUN_ENVIRONMENT_LINUX)
	if (m_pLCD != NULL)
		delete m_pLCD;
#endif // defined(RUN_ENVIRONMENT_LINUX)

	if (m_pLCD) {
		delete m_pLCD;
	}
	if (m_pScreenBuffer != NULL) {
		delete m_pScreenBuffer;
	}
	if (m_pMsgQ != NULL) {
		delete m_pMsgQ;
	}
	m_pMainWnd = NULL;
	if (m_pCaret != NULL) {
		delete m_pCaret;
	}
	if (m_pTimerQ != NULL) {
		delete m_pTimerQ;
	}
	m_pTopMostWnd = NULL;
	if (m_pKeyMap != NULL) {
		delete m_pKeyMap;
	}
	if (m_pClock != NULL) {
		delete m_pClock;
	}
	if (m_pSysBar != NULL) {
		delete m_pSysBar;
	}
	if (m_pImageMgt != NULL) {
		delete m_pImageMgt;
	}

#if defined (CHINESE_SUPPORT)
	if (m_pIMEWnd != NULL) {
		delete m_pIMEWnd;
	}
#endif // defined(CHINESE_SUPPORT)

#if defined (RUN_ENVIRONMENT_WIN32)
	m_pLCD->Win32Fini ();
#endif //defined()

#if defined (RUN_ENVIRONMENT_LINUX)
	m_pLCD->LinuxFini ();
#endif //defined()
}

// 调用一次切换一次显示模式，并返回切换后的模式
BOOL OApp::ReverseShow(void)
{
	m_bReverseShow = (m_bReverseShow ? 0 : 1);
	return m_bReverseShow;
}

// 绘制窗口
void OApp::PaintWindows (OWindow* pWnd)
{
	// 调用主窗口的绘制函数
	pWnd->Paint (m_pScreenBuffer);

#if defined (CHINESE_SUPPORT)

	// 调用输入法窗口的绘制函数
	m_pIMEWnd->Paint (m_pScreenBuffer);
#endif // defined(CHINESE_SUPPORT)

	// 绘制TopMost窗口
	if (m_pTopMostWnd != NULL && IsWindowValid (m_pTopMostWnd)) {
		//DebugPrintf("Debug: OApp::PaintWindows m_pTopMostWnd->type is %s\n",
		//	GetWindowTypeName(m_pTopMostWnd->GetWndType()));
		m_pTopMostWnd->Paint(m_pScreenBuffer);
	}

	// 绘制系统状态条
	m_pSysBar->Show(m_pScreenBuffer);

#if defined (MOUSE_SUPPORT)
	// 绘制时钟按钮
	m_pClock->ShowClockButton(m_pScreenBuffer);
#endif // defined(MOUSE_SUPPORT)

	// 将绘制好的buf传给真正的屏幕
	m_pLCD->Copy (*m_pScreenBuffer, 0);

	// 绘制脱字符
	m_pCaret->DrawCaret (m_pLCD, m_pScreenBuffer);

	Show ();
}

#if defined (RUN_ENVIRONMENT_LINUX)
BOOL OApp::Create (OWindow* pMainWnd, LCD* pLCD_forXwin, FnEventProcess fnEventProc)
{
	if (pMainWnd == NULL || pLCD_forXwin == NULL || fnEventProc == NULL) {
		DebugPrintf ("ERROR: OApp::Create parameter == NULL!\n");
		return FALSE;
	}

	m_pLCD = pLCD_forXwin;
	m_pMainWnd = pMainWnd;
	m_fnEventProcess = fnEventProc;
	m_pMainWnd->m_pApp = this;

	// 创建按键宏与按键值对照表
	if (! m_pKeyMap->Load((char*)KEY_MAP_FILE )) {
		DebugPrintf ("ERROR: OApp::Create m_pKeyMap->Load KEY_MAP_FILE fail!\n");
		return FALSE;
	}

	// 显示开始画面
	ShowStart ();

#if defined (CHINESE_SUPPORT)

	// 创建输入法窗口
	if (! m_pIMEWnd->Create(this)) {
		DebugPrintf ("ERROR: OApp::Create m_pIMEWnd->Create fail!\n");
		return FALSE;
	}
#endif // defined(CHINESE_SUPPORT)

	m_pMainWnd->UpdateView (m_pMainWnd);
	return TRUE;
}
#endif // defined(RUN_ENVIRONMENT_LINUX)

#if defined (RUN_ENVIRONMENT_WIN32)
BOOL OApp::Create (OWindow* pMainWnd, HWND hWnd, FnEventProcess fnEventProc)
{
	if (pMainWnd == NULL || hWnd == NULL) {
		DebugPrintf ("ERROR: OApp::Create parameter == NULL!\n");
		return FALSE;
	}

	m_hWnd = hWnd;

	m_pLCD = new LCD;
	m_pLCD->Win32Init (SCREEN_W, SCREEN_H);

	m_pMainWnd = pMainWnd;
	m_fnEventProcess = fnEventProc;
	m_pMainWnd->m_pApp = this;

	// 创建按键宏与按键值对照表
	if (! m_pKeyMap->Load(KEY_MAP_FILE)) {
		DebugPrintf ("ERROR: OApp::Create m_pKeyMap->Load KEY_MAP_FILE fail!\n");
		return FALSE; 
	}

	// 显示开始画面
	ShowStart ();

#if defined (CHINESE_SUPPORT)
	// 创建输入法窗口
	if (! m_pIMEWnd->Create(this)) {
		DebugPrintf ("ERROR: OApp::Create m_pIMEWnd->Create fail!\n");
		return FALSE;
	}
#endif // defined(CHINESE_SUPPORT)

	m_pMainWnd->UpdateView (m_pMainWnd);
	return TRUE;
}
#endif // defined(RUN_ENVIRONMENT_WIN32)

BOOL OApp::Run ()
{
	int nMsgInfo = 1;
	O_MSG msg;

	while (nMsgInfo != 0)
	{
		int nMsgInfo = m_pMsgQ->GetMsg (&msg);

		if (nMsgInfo == 1) {
			DespatchMsg (&msg);
		}
		else if (nMsgInfo == -1) {
			Idle();
		}
	}

	// run循环结束，退出程序
	return TRUE;
}

// 专用于Microsoft Windows和Linux XWindows的仿真环境
BOOL OApp::RunInEmulator()
{
	int nMsgInfo = 1;
	O_MSG msg;

	while (nMsgInfo != -1)
	{
		nMsgInfo = m_pMsgQ->GetMsg (&msg);
		if (nMsgInfo == 1)
		{
			DespatchMsg (&msg);
		}
		else if (nMsgInfo == 0)
		{
			// 退出程序
			return FALSE;
		}
	}

	Idle();
	return TRUE;
}

// 消息队列空状态下的处理
void OApp::Idle()
{
	m_pTimerQ->CheckTimer(this);	// 检查定时器队列，有到时的则发送定时器消息

	// 如果CLOCK窗口处于打开状态，则不更新脱字符
	if (m_pClock->IsEnable()) {
		if (m_pClock->Check (m_pLCD, m_pScreenBuffer)) {
			Show ();
		}
	}
	else {
		m_pCaret->Check (m_pLCD, m_pScreenBuffer);	// 更新脱字符
	}

	// 空闲处理
	OnIdle();
}

// 显示开机画面
void OApp::ShowStart()
{
}

// 空闲处理
void OApp::OnIdle()
{
#if defined (RUN_ENVIRONMENT_WIN32)
	Sleep (10); //避免界面刷新过于频繁
#endif // defined(RUN_ENVIRONMENT_WIN32)

#if defined (RUN_ENVIRONMENT_LINUX)
	//usleep(1000); //避免界面刷新过于频繁
#endif // defined(RUN_ENVIRONMENT_LINUX)
}

// 蜂鸣器鸣叫指示检测到按键按键消息
void OApp::Beep()
{
}

// 刷新显示
void OApp::Show()
{
#if defined (RUN_ENVIRONMENT_LINUX)
	m_pLCD->Show ();
#endif // defined(RUN_ENVIRONMENT_LINUX)

#if defined (RUN_ENVIRONMENT_WIN32)
	m_pLCD->Show (m_hWnd, m_bReverseShow);
#endif // defined(RUN_ENVIRONMENT_WIN32)
}

void OApp::PostKeyMsg (WORD wKeyValue)
{
	// 获得特殊功能键的状态
	int nKeyMask = 0x00000000;
	if (m_pSysBar->GetCaps()) {
		nKeyMask |= CAPSLOCK_MASK;
	}

#if defined (RUN_ENVIRONMENT_LINUX)
	// Win32下的 KEY_VALUE 无论大小写，都是大写ASCII码值
	// 为了跟Win32下的 KEY_X 宏定义统一，XWindows大写全部启用上档键
	if (wKeyValue >= KEY_A && wKeyValue <= KEY_Z) {
		nKeyMask |= SHIFT_MASK;
	}
	if (wKeyValue >= (KEY_A + 0x20) && wKeyValue <= (KEY_Z + 0x20)) {
		wKeyValue -= 0x20;
	}
#endif // defined(RUN_ENVIRONMENT_LINUX)

#if defined (RUN_ENVIRONMENT_WIN32)
	if (GetKeyState(VK_SHIFT) & 0x80) {
		nKeyMask |= SHIFT_MASK;
	}
#endif // defined(RUN_ENVIRONMENT_WIN32)

	O_MSG msg;
	msg.pWnd    = m_pMainWnd;
	msg.message = OM_KEYDOWN;
	msg.wParam  = wKeyValue;
	msg.lParam  = nKeyMask;
	m_pMsgQ->PostMsg(&msg);
}

void OApp::PostMouseMsg (int nType, int x, int y)
{
	if ((nType != OM_LBUTTONDOWN) &&
		(nType != OM_LBUTTONUP) &&
		(nType != OM_MOUSEMOVE))
		return;

	O_MSG msg;
	msg.pWnd    = m_pMainWnd;
	msg.message = nType;
	msg.wParam  = x;
	msg.lParam  = y;
	m_pMsgQ->PostMsg(&msg);
}

// 发送消息
// 区分出OM_PAINT消息单独处理
int OApp::DespatchMsg (O_MSG* pMsg)
{
	
#if defined (MOUSE_SUPPORT)

	// 区分出鼠标消息单独处理
	if ((pMsg->message == OM_LBUTTONDOWN) ||
		(pMsg->message == OM_LBUTTONUP)   ||
		(pMsg->message == OM_MOUSEMOVE))
	{
		if (pMsg->message == OM_LBUTTONDOWN)
		{
			// 首先处理点击时钟按钮的消息
			if (m_pClock->PtProc (pMsg->wParam, pMsg->lParam))
			{
				PaintWindows (m_pMainWnd);
				return 0;
			}

			// 鼠标点击SystemBar上大小写转换按钮的处理
			if (m_pSysBar->PtProc (pMsg->wParam, pMsg->lParam)) {
				return 0;
			}
		}

#if defined (CHINESE_SUPPORT)
		// 如果输入法窗口处于打开状态，则首先传递给输入法窗口处理
		if (IsIMEOpen())
		{
			if (m_pIMEWnd->PtInWindow (pMsg->wParam, pMsg->lParam))
			{
				m_pIMEWnd->PtProc (pMsg->pWnd, pMsg->message, pMsg->wParam, pMsg->lParam);
				return 0;
			}
		}
#endif // defined(CHINESE_SUPPORT)

		if (IsWindowValid (pMsg->pWnd))
		{
			 return pMsg->pWnd->PtProc (pMsg->pWnd, pMsg->message, pMsg->wParam, pMsg->lParam);
		}
		else {
			DebugPrintf("ERROR: OApp::DespatchMsg pMsg->pWnd is not valid!\n");
		}
	}

	// 区分出OM_PAINT消息单独处理
	if (pMsg->message == OM_PAINT)
	{
		PaintWindows (pMsg->pWnd);
		return 0;
	}

	return (SendMsg (pMsg));
#else	
	if (pMsg->message == OM_PAINT)
	{
		PaintWindows (pMsg->pWnd);
		return 0;
	}

	return (SendMsg (pMsg));
#endif // defined(MOUSE_SUPPORT)

}

#if defined (RUN_ENVIRONMENT_WIN32)
// 该函数仅用于windows下仿真,在Win32下替换掉DispatchMessage函数
void OApp::InsertMsgToMonoGUI (MSG* pMSG)
{
	// 只插入键盘消息、鼠标左键按下消息和WM_PAINT消息
	if (pMSG->message == WM_KEYDOWN)
	{
		if (m_pClock->IsEnable())
		{
			m_pClock->Enable(FALSE);
		}
		else if (pMSG->wParam == KEY_CAPS_LOCK)
		{
			// 翻转Caps的状态
			if (m_pSysBar->GetCaps()) {
				m_pSysBar->SetCaps(FALSE);
			}
			else {
				m_pSysBar->SetCaps(TRUE);
			}

			// 重绘窗口
			PaintWindows (m_pMainWnd);
		}
		else
		{
			if (m_bClockKeyEnable)
			{
				if (pMSG->wParam == KEY_CLOCK)
				{
					if (m_pClock->IsEnable()) {
						m_pClock->Enable(FALSE);
					}
					else {
						m_pClock->Enable(TRUE);
						// 重绘窗口，擦掉时钟按钮
						PaintWindows (m_pMainWnd);
					}
				}
				else
				{
					PostKeyMsg (pMSG->wParam);
				}
			}
			else
			{
				PostKeyMsg (pMSG->wParam);
			}
		}

		Beep();
	}

#if defined (MOUSE_SUPPORT)
	// 只响应鼠标左键
	if ((pMSG->message == WM_LBUTTONDOWN) ||
		(pMSG->message == WM_LBUTTONUP)   ||
		(pMSG->message == WM_MOUSEMOVE))
	{
		int x = (WORD)(pMSG->lParam);
		int y = (WORD)(((DWORD)(pMSG->lParam) >> 16) & 0xFFFF);

		// 根据屏幕的缩放状况进行坐标变换
		x = x / SCREEN_MODE;
		y = y / SCREEN_MODE;

		O_MSG msg;
		msg.pWnd	= m_pMainWnd;
		msg.wParam  = x;
		msg.lParam  = y;

		switch (pMSG->message)
		{
		case WM_LBUTTONDOWN:
			msg.message = OM_LBUTTONDOWN;
			break;
		case WM_LBUTTONUP:
			msg.message = OM_LBUTTONUP;
			break;
		case WM_MOUSEMOVE:
			msg.message = OM_MOUSEMOVE;
			break;
		}

		m_pMsgQ->PostMsg (&msg);
	}
#endif // defined(MOUSE_SUPPORT)

	// 如果CLOCK窗口处于打开状态，
	// 除MonoGUI系统状态条所需消息之外的所有消息都会被拦截
	if (m_pClock->IsEnable()) {
		return;
	}

	// Clock关闭状态下允许系统WM_PAINT消息
	if (pMSG->message == WM_PAINT)
	{
		O_MSG msg;
		msg.pWnd	= m_pMainWnd;
		msg.message = OM_PAINT;
		msg.wParam  = 0;
		msg.lParam  = 0;
		m_pMsgQ->PostMsg (&msg);
	}
}
#endif // defined(RUN_ENVIRONMENT_WIN32)

// 下面的函数调用成员类的相应函数实现

#if defined (CHINESE_SUPPORT)
// 打开输入法窗口(打开显示，创建联系)
BOOL OApp::OpenIME (OWindow* pWnd)
{
	return (m_pIMEWnd->OpenIME(pWnd));
}
#endif // defined(CHINESE_SUPPORT)

#if defined (CHINESE_SUPPORT)
// 关闭输入法窗口(关闭显示，断开联系)
BOOL OApp::CloseIME (OWindow* pWnd)
{
	return (m_pIMEWnd->CloseIME(pWnd));
}
#endif // defined(CHINESE_SUPPORT)

// 显示系统信息
BOOL OApp::ShowHint (int nIcon, char* psInfo)
{
	// 计算显示文字可能会占用的空间
	int nWidth = 0;
	int nHeight = 0;
	BYTE sTemp [WINDOW_CAPTION_BUFFER_LEN];

	// 最多不能超过四行文本
	int i;
	for (i = 0; i < 4; i++)
	{
		memset (sTemp, 0x0, sizeof(sTemp));
		if (GetLine (psInfo, (char *)sTemp, sizeof(sTemp), i))
		{
			int nDesplayLength = GetDisplayLength ((char *)sTemp,
				sizeof(sTemp)-1);
			if (nDesplayLength > nWidth) {
				nWidth = nDesplayLength;
			}

			nHeight += 14;
		}
	}

	if (nWidth > (SCREEN_W - 40)) {
		nWidth = SCREEN_W - 40;
	}

	int nTextLeft = 10;
	int tw = nWidth + 24;
	int th = nHeight + 42;

	// 留出图标的位置
	if (nIcon > 0) {
		tw += 30;
		nTextLeft += 30;
	}
	if (tw < 80) {
		tw = 80;
	}

	int w;
	int h;
	BYTE* fb = m_pLCD->GetBuffer(&w, &h);
	int tx = (w - tw) / 2;
	int ty = (h - th) / 2;

	// 清空背景区域并绘制边框
	m_pLCD->FillRect(tx, ty, tw, th, 0);
	m_pLCD->HLine   (tx, ty, tw, 1);
	m_pLCD->HLine   (tx, ty+th-1, tw, 1);
	m_pLCD->VLine   (tx, ty, th, 1);
	m_pLCD->VLine   (tx+tw-1, ty, th, 1);

	// 绘制图标
	FOUR_COLOR_IMAGE* pIcon = NULL;
	switch (nIcon)
	{
	case OMB_ERROR:
		pIcon = &g_4color_Icon_Error;
		break;

	case OMB_EXCLAMATION:
		pIcon = &g_4color_Icon_Exclamation;
		break;

	case OMB_QUESTION:
		pIcon = &g_4color_Icon_Question;
		break;

	case OMB_INFORMATION:
		pIcon = &g_4color_Icon_Information;
		break;

	case OMB_BUSY:
		pIcon = &g_4color_Icon_Busy;
		break;

	case OMB_PRINT:
		pIcon = &g_4color_Icon_Printer;
		break;
	}
	m_pLCD->DrawImage (tx+10,ty+18,23,23,*pIcon,0,0, LCD_MODE_NORMAL);

	// 拆分并绘制文本
	// 最多不能超过四行文本
	for (i = 0; i < 4; i++)
	{
		memset (sTemp, 0x0, sizeof(sTemp));
		if (GetLine (psInfo, (char *)sTemp, sizeof(sTemp), i)) {
			m_pLCD->TextOut (tx+nTextLeft,ty+21+i*14,sTemp,sizeof(sTemp)-1, LCD_MODE_NORMAL);
		}
	}

	Show ();
	return TRUE;
}

// 关闭系统提示
BOOL OApp::CloseHint()
{
	m_pLCD->Copy (*m_pScreenBuffer, 0);
	return TRUE;
}

// 直接调用消息处理函数；
int OApp::SendMsg (O_MSG* pMsg)
{
	if (IsWindowValid (pMsg->pWnd))
	{
		return pMsg->pWnd->Proc (pMsg->pWnd, pMsg->message, pMsg->wParam, pMsg->lParam);
	}
	else {
		DebugPrintf("ERROR: OApp::SendMsg pMsg->pWnd is not valid!\n");
	}

	return 0;
}

// 向消息队列中添加一条消息；
// 如果消息队列满（消息数量达到了MESSAGE_MAX 所定义的数目），则返回失败；
BOOL OApp::PostMsg (O_MSG* pMsg)
{
	return m_pMsgQ->PostMsg (pMsg);
}

// 向消息队列中添加一条退出消息；
BOOL OApp::PostQuitMsg()
{
	O_MSG msg;
	msg.pWnd = NULL;
	msg.message = OM_QUIT;
	msg.wParam = 0;
	msg.lParam = 0;
	return m_pMsgQ->PostMsg (&msg);
}

// 在消息队列中查找指定类型的消息；
// 如果发现消息队列中有指定类型的消息，则返回TRUE；
// 该函数主要用在定时器处理上。CheckTimer函数首先检查消息队列中有没有相同的定时器消息，如果没有，再插入新的定时器消息
BOOL OApp::FindMsg (O_MSG* pMsg)
{
	return m_pMsgQ->FindMsg (pMsg);
}

// 根据窗口的脱字符信息设置系统脱字符的参数；
BOOL OApp::SetCaret (CARET* pCaret)
{
	return m_pCaret->SetCaret (pCaret);
}

// 添加一个定时器；
// 如果当前定时器的数量已经达到TIMER_MAX所定义的数目，则返回FALSE；
// 如果发现一个ID与当前定时器相同的定时器，则直接修改该定时器的设定；
BOOL OApp::SetTimer (OWindow* pWindow, int nTimerID, int interval)
{
	return m_pTimerQ->SetTimer (pWindow, nTimerID, interval);
}

// 删除一个定时器；
// 根据TimerID删除
BOOL OApp::KillTimer (int nTimerID)
{
	return m_pTimerQ->KillTimer (nTimerID);
}

#if defined (CHINESE_SUPPORT)
// 看输入法窗口是否处于打开状态
BOOL OApp::IsIMEOpen ()
{
	return m_pIMEWnd->IsIMEOpen ();
}
#endif // defined(CHINESE_SUPPORT)

// 设置TopMost窗口
BOOL OApp::SetTopMost(OWindow* pWindow)
{
	if (pWindow) {
		DebugPrintf("Debug: OApp::SetTopMost: pWindow->type is %s\n",
			GetWindowTypeName(pWindow->GetWndType()));
	}

	if (IsWindowValid(pWindow))	{
		m_pTopMostWnd = pWindow;
		return TRUE;
	}
	else {
		m_pTopMostWnd = NULL;
		DebugPrintf ("ERROR: OApp::SetTopMost pWindow is not Valid!\n");
		return FALSE;
	}
}

// 检验一个窗口指针是否有效
BOOL OApp::IsWindowValid (OWindow* pWindow)
{
	if (pWindow == NULL) {
		DebugPrintf ("ERROR: OApp::IsWindowValid parameter pWindow is NULL!\n");
		return FALSE;
	}

	WORD wType = pWindow->GetWndType();
	if ((wType > WND_VALID_BEGIN) && (wType < WND_VALID_END)) {	// 判断TopMost窗口的有效性
		return TRUE;
	}

	DebugPrintf ("ERROR: OApp::IsWindowValid met A INVALID WINDOW (Maybe RELEASED)!\n");
	return FALSE;
}

// 使能/禁止显示时钟
// 注：显示时钟使能时，按“当前时间”键打开时钟窗口。
// “当前时间”按键消息将不能发送给任何一个窗口。
// 机器自检时，需要禁止始终，以保证自检窗口可以接收到“当前时间”的按键消息
BOOL OApp::ClockKeyEnable (BOOL bEnable)
{
	// 如果禁止显示时钟，则首先关闭时钟窗口
	if (! bEnable)
	{
		if (m_pClock->IsEnable()) {
			m_pClock->Enable (FALSE);
		}
	}
	BOOL bTemp = m_bClockKeyEnable;
	m_bClockKeyEnable = bEnable;
	return bTemp;
}

// 设置时钟的显示位置
BOOL OApp::SetClockPos (int x, int y)
{
	return m_pClock->SetClockPos (x, y);
}

#if defined (MOUSE_SUPPORT)
// 设置时钟按钮的位置
BOOL OApp::SetClockButton (int x, int y)
{
	return m_pClock->SetClockButton (x, y);
}
#endif // defined(MOUSE_SUPPORT)

/* END */