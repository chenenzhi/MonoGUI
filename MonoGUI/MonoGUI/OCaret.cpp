////////////////////////////////////////////////////////////////////////////////
// @file OCaret.cpp
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#include "MonoGUI.h"

OCaret::OCaret()
{
    m_Caret.bValid = FALSE;		// 是否使用脱字符
    m_Caret.x = 0;				// 位置
    m_Caret.y = 0;
    m_Caret.w = 0;				// 宽高
    m_Caret.h = 0;
    m_Caret.bFlash = FALSE;			// 是否闪烁
    m_Caret.bShow  = TRUE;			// (第一次出现应该处于显示状态)
    m_Caret.lTimeInterval = 500;	// 闪烁的时间间隔(一般采用500毫秒)
    m_lLastTime = 0;
}

OCaret::~OCaret()
{
}

// 根据窗口的脱字符信息设置系统脱字符的参数；
BOOL OCaret::SetCaret (CARET* pCaret)
{
	if (pCaret == NULL)
		return FALSE;

	memcpy (&m_Caret, pCaret, sizeof(CARET));
	m_lLastTime = sys_clock ();
	return TRUE;
}

// 更新脱字符的显示。
// 如果脱字符定时器到时了，则将主缓中脱字符区域的图像以适当方式送入FrameBuffer的对应位置；
BOOL OCaret::Check (LCD* pLCD, LCD* pBuf)
{
	if (m_Caret.bValid)
	{
		ULONGLONG lNow = sys_clock();

		if ((lNow - m_lLastTime) >= m_Caret.lTimeInterval)
		{
			if (m_Caret.bFlash)
			{
				if (m_Caret.bShow)
				{
					// 反白显示
					DrawCaret(pLCD, pBuf);
					m_Caret.bShow = FALSE;
				}
				else
				{
					// 正常显示
					DrawCaret(pLCD, pBuf);
					m_Caret.bShow = TRUE;
				}
			}
			else
			{
				// 反白显示
				DrawCaret(pLCD, pBuf);
			}
			m_lLastTime = lNow;
		}
		return TRUE;
	}
	return FALSE;
}

void OCaret::DrawCaret (LCD* pLCD, LCD* pBuf)
{
	if(m_Caret.bValid)
	{
		int nMode;
		if (m_Caret.bShow == TRUE)
		{
			nMode = LCD_MODE_INVERSE;
		}
		else
		{
			nMode = LCD_MODE_NORMAL;
		}

		pLCD->BitBlt(m_Caret.x, m_Caret.y, m_Caret.w, m_Caret.h,
			*pBuf, m_Caret.x, m_Caret.y, nMode);
	}
}

/* END */
