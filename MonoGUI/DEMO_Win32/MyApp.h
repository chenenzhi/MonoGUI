////////////////////////////////////////////////////////////////////////////////
// @file MyApp.h
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#if !defined (__MYAPP_H__)
#define __MYAPP_H__

class MyApp : public OApp
{
	//int  m_nSysMode;             // 当前模式
	//int  m_nCurLockPos;          // 当前锁位
	//BOOL m_bLockChangeEnable;    // 是否允许改变当前锁位

public:
	MyApp();
	virtual ~MyApp();

	// 绘制
	virtual void PaintWindows (OWindow* pWnd);

	// 显示开机画面
	virtual void ShowStart();
};

#endif // !defined (__MYAPP_H__)
