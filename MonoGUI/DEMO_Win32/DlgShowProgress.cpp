////////////////////////////////////////////////////////////////////////////////
// @file DlgShowProgress.cpp
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"

CDlgShowProgress::CDlgShowProgress()
{
}

CDlgShowProgress::~CDlgShowProgress()
{
}

// 初始化
void CDlgShowProgress::Init()
{
	m_pApp->PaintWindows (m_pApp->m_pMainWnd);
	OProgressBar* pPgs = (OProgressBar *)FindChildByID (102);
	pPgs->SetRange (100);
	int i;
	for (i = 0; i <= 100; i++)
	{
		pPgs->SetPos (i);

		// 延时一会儿
		Delay(10);
	}
	// 延时一会儿
	Delay(500);

	// 关闭本窗口
	O_MSG msg;
	msg.pWnd = this;
	msg.message = OM_CLOSE;
	msg.wParam = 0;
	msg.lParam = 0;
	m_pApp->PostMsg (&msg);
}

/* END */