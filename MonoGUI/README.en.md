Windows:
Install VS2022 (requires desktop development and MFC support), open MonoGUI.sln to compile and run directly.

Under ubuntu:
1, Copy the DEMO_Ubuntu22.04, dtm, zhlib, MonoGUI four directory to Linux, pay attention to do not change the file name case;
2. Enter the MonoGUI directory and execute the make command to generate the monogui.a file;
3. Enter the DEMO_Ubuntu22.04 directory, execute the make command to generate the executable program ‘demo’ and it will be copied to the upper directory;
4. Go back to the upper directory of DEMO_Ubuntu22.04 and type './demo' to execute it.

Notice (for Linux):
1. Do not convert the source-codes to utf-8 format!
2. Do not add the -input-charset=GBK compilation option!
3. Chinese Pinyin table index file (.idx files under zhlib directory) is generated under windows, then copy it to Linux to use it.
4. The Picture Converter tools, DTM (dialog box template) editor and other tools are only available for windows version (also provides the source code).